## megatools，Mega.nz client
- 官方，Megatools，mega client cli
    https://megatools.megous.com/

- 下載 Megatools
    https://megatools.megous.com/builds/experimental/

- Megatools Doc
    https://megatools.megous.com/man/megatools.html

- usage
    https://megatools.megous.com/man/megatools-reg.html

## 3rd Megatools Wrapper
- [py] Megatools, communication to mega with Megatools, only implement dl function
https://github.com/Harkame/Megatools

- [py] mega.py, communication to mega with http-req
https://pypi.org/project/mega.py/

## getnada，tempMail
- Official Site
    https://getnada.com/
    https://getnada.com/api/v1
    
- [py] getnada-api, nada, not work
    https://raw.githubusercontent.com/wahid/getnada-api/master/nada.py

- [py] https://github.com/fyxme/pynada, work

## guerrillamail，tempMail
- https://www.guerrillamail.com/zh-hant/inbox

## Dev Note
- 傳遞 subprocess 的命令時，若路徑或檔名有空白時，cmd 改用 array 形式，避免出現參數錯誤

## issue
- mega 提供的連結是檔案下載的引導頁面，若雲端檔案`不包含副檔名` 或 `不支援的影片類型`，就會被當作一般檔案下載，
    例如，https://mega.nz/file/ih5Fya7B#-fnclC66OF93gmp8_wZP7RCV1fEnh5mMwIZSHh9dWEY 
    該連結實際上連接到一個下載的頁面，而不是直接下載檔案

    向該連結發起 req 後，server 端返回的 mine 類型是 text/html，而不是 file
    實際的下載連結是被 mega 加密且隱藏的

- 對於一個具有副檔名的影片連結，可以將
    https://mega.nz/file/ih5Fya7B#-fnclC66OF93gmp8_wZP7RCV1fEnh5mMwIZSHh9dWEY 的 file 改為 embed，
    https://mega.nz/embed/ih5Fya7B#-fnclC66OF93gmp8_wZP7RCV1fEnh5mMwIZSHh9dWEY 取得可撥放該影片的網頁 
    (網頁自帶撥放器)

    注意，具副檔名的連結才可以撥放，否則，embed 的連結，依舊會跳轉到下載檔案的頁面中


# TODO
- 混淆檔名+副檔名上傳檔案 + webview 瀏覽影片
    缺點 flv 檔案類型無法撥放

