import os
import subprocess
from typing import Optional

from config import Config
from utils.logger import y2bLogger

def isy2bExist():
    if not os.path.isfile(Config.y2b): raise ValueError("[Error] youtubedl.exe not found ...")

def y2bDownload(url:str) -> Optional[str]:
    output:str = ""
    filename = subprocess.check_output(f"youtubedl --get-filename {url}").decode("utf-8").strip()

    if not (".mp4" in filename):
        filename += ".mp4"

    p = subprocess.Popen(["youtubedl", "-f", "mp4", url, "--output", filename], stdout=subprocess.PIPE)
    
    while True:
        # ==== 當輸出不為 None 時，印出訊息 ====
        line = p.stdout.readline()

        if len(line) > 0:
            y2bLogger.info(line.decode("utf-8").strip())

        # ==== 判斷命令是否結束 ====
        if p.poll() != None:
            break
    
    if os.path.isfile(filename):
        y2bLogger.info("download done ...")
        return filename
    else:
        y2bLogger.info(f"download fail, {filename} not existed ...")
        return None

isy2bExist()