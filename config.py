from dataclasses import dataclass
from os.path import isfile

@dataclass
class Config:
    # executable = "C:/Users/Administrator/Desktop/myMega_0124/exec/megatools-1.11.0-git-20200503-win64/megatools.exe"
    # y2b = "C:/Users/Administrator/Desktop/myMega_0124/exec/youtubedl.exe"
    
    executable = "C:/app/megatools-1.11.0-git-20200503-win64/megatools.exe"
    y2b = "C:/youtubedl/youtubedl.exe"

    user = "" 
    pwd = ""

def checkConfig():
    if any([
        Config.executable == "",
        Config.y2b == "",
        Config.user == "",
        Config.pwd == "",
        not isfile(Config.executable),
        not isfile(Config.y2b),
    ]): raise ValueError("setting value wrong in Config")

checkConfig()