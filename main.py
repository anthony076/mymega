import os, time, subprocess
from typing import Optional, List

from config import Config
from sites.y2b import y2bDownload

from utils.logger import mainLogger
from utils.myMegatools import MyMegaTools
from utils.mydb import DbHandler, NewAccount, NewItem

db = DbHandler()

def upload(filename:str, rFolderName:str):
    mega = MyMegaTools(executable=Config.executable, user=Config.user, pwd=Config.pwd)
    freeSpace = int(mega.diskInfo()["FREE"][:-2])

    if freeSpace < 100:
        # ==== space not enough ====
        newAccount:dict = mega.reg(n=1)
        mega.setNewAccount(usr=newAccount["mail"], pwd=newAccount["pwd"])
        accountRow = db.addRow(NewAccount(name=newAccount["name"], mail=newAccount["mail"], pwd=newAccount["pwd"]), table="account")

        mega.createFolder(rFolderName)
        remotePath = mega.upload( filePath=filename, rFolderName=rFolderName )
        link:str = mega.getFileLink(filename)

        # ==== write to db ====
        md5 = DbHandler._str2md5(filename)
        b64 = DbHandler._str2b64(filename)
        
        newFile = NewItem(
            name=filename, md5=md5, b64=b64, 
            remotePath=remotePath, remoteLink=link, 
            account=accountRow
        )

        itemRow = db.addRow(newFile, table="item")

        # ==== delete loacal file ====
        if (accountRow != None) and (itemRow != None):
            os.remove(filename)

def main():
    
    filename = y2bDownload("https://www.youtube.com/watch?v=kdy_fe71-0I")
    
    if filename: 
        upload(filename, "tmp")
    else:
        mainLogger.info("filename not found ...")

if __name__ == "__main__":
    main()



