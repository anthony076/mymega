import logging

class Mylogger:
    def __init__(self, name, enableDebug:bool=False):
        self.name:str = name
        self.enableDegub:bool = enableDebug
        self.debugFmt = logging.Formatter('[%(asctime)s][%(funcName)s] %(message)s')
        self.infoFmt = logging.Formatter('[%(asctime)s] %(message)s')

    def createLogger(self):
        # get logger
        newlogger = logging.getLogger(self.name)
        handler = logging.StreamHandler()

        # set level and format
        if self.enableDegub:
            newlogger.setLevel(logging.DEBUG)
            handler.setFormatter(self.debugFmt)
        else:
            newlogger.setLevel(logging.INFO)
            handler.setFormatter(self.infoFmt)

        newlogger.addHandler(handler)

        return newlogger

mainLogger = Mylogger("main", enableDebug=False).createLogger()
nadaLogger = Mylogger("nada", enableDebug=False).createLogger()
dbLogger = Mylogger("db", enableDebug=False).createLogger()
megaLogger = Mylogger("mega", enableDebug=False).createLogger()
y2bLogger = Mylogger("y2b", enableDebug=False).createLogger()