"""
Usage of Megatools-cli, communicate with subprocess

限制，megatools 對於無附檔名的檔案，無法區分是檔案或是資料夾
"""
import os, time, subprocess
import secrets, string
from typing import Optional, Union
from pathlib import PurePosixPath, Path

from pyquery import PyQuery as pq

from config import Config
from utils.nada import Pynada
from utils.logger import megaLogger

def genMail():
    mail = genPass(n=6, digit=False).lower() + "@getnada.com"
    return mail

def genPass(n:int=20, digit=True):
    alphabet = string.ascii_letters

    if digit:
        alphabet += string.digits

    return ''.join(secrets.choice(alphabet) for i in range(n)) 

class MyMegaTools:
    def __init__(self, executable:str, user:str=None, pwd:str=None):
        self.executable = executable
        self.user = user
        self.pwd = pwd
        self.fileList = []
        self._checkExecutable()

        if user and pwd:
            self.listFiles(display=False, reload=True)

    def setNewAccount(self, usr:str, pwd:str):
        self.user = usr
        self.pwd = pwd
        
        # update fileList for new user
        self.listFiles(display=False, reload=True)

        return True
    
    def _checkExecutable(self):
        assert os.path.isfile(self.executable), "[Error] megatools.exe not found"
 
    def _exec(self, cmd:str, display:bool=True) -> list:
        result = []
        
        process:Popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   
        while True:
            line = process.stdout.readline()

            if len(line) > 0:

                msg = line.decode("utf-8").strip()

                # show result
                if display: print(msg)

                # get result
                if "megatools reg" in msg:
                    return [msg.split(" ")[3]]
                else:
                    result.append(msg)

            #if not line: 
            if process.poll() != None: 
                break

        return result

    def _filter_listFiles(self, x) -> str:
        ignoreList = ["Contacts", "Inbox", "Trash"]
        
        for item in ignoreList:
            if item in x:
                return

        return x

    def listFiles(self, display:bool=True, reload=False) -> list[str]:
        """
        # Need to set reload=True if use upload/download relatived functions.
        # reload parameter will refresh local file-system, and will slower listFiles()
        """
        
        cmd = f"megatools ls -u {self.user} -p {self.pwd}"

        if reload:
            cmd += " --reload"

        result:list = self._exec(cmd, display=False)

        # fileter result 
        result = list(map(self._filter_listFiles , result))
        result = list(filter(lambda x: x != None, result))

        result.sort()
        self.fileList = result

        if display:
            megaLogger.info(self.fileList)

        return self.fileList

    def _format_distInfo(self, raw:list) -> None:
        result = dict()

        for item in raw:
            # remove space
            item = "".join(c for c in item if c != " ")
            name = item.split(":")[0]
            value = item.split(":")[1]

            result[name.upper()] = value+"gb"

        return result

    def diskInfo(self) -> dict:
        cmd = f"megatools df --gb -u {self.user} -p {self.pwd} --reload"
        result :list = self._exec(cmd, display=False)

        return self._format_distInfo(result)

    def find(self, fn:str, display:bool=False, sort:str="all") -> list:
        """
        Usage:
            mymega.find("111")                    # 顯示所有符合的目錄和檔案
            mymega.find("111.txt", sort="file")   # 返回檔案的遠端路徑，但只保留檔案類型的結果
            mymega.find("ttt", sort="folder")     # 返回目錄的遠端路徑，只保留目錄類型的結果
            mymega.find("ttt", sort="file")       # 返回目錄內所有的檔案的遠端路徑，只保留檔案類型的結果
            mymega.find("111.txt", sort="folder") # 返回包含該檔案的所有資料夾目錄
        """
        result = []
        megaLogger.debug(f"findkey: {fn}")

        for item in self.fileList:
            if fn in item:
                if display: print(item)
                result.append(item)
    
        if sort == "all":
            return result
        elif (sort == "folder" ) and ("." in fn):
            return list(map(lambda fPath: str(PurePosixPath(fPath).parents[0]), result))
        elif sort == "folder":
            # keep folder path only
            return list(filter(lambda x: not("." in x), result))
        elif sort == "file":
            # keep file path only
            return list(filter(lambda x: "." in x, result))

    def createFolder(self, newFolderName:str) -> str:
        """
        create remote folder at /Root
        Ex: megatools mkdir /Root/MyNewFolder
        Ex: mega.createFolder("tmp/666") or mega.createFolder("/Root/tmp/666") are acceptable
        Ex: mega.createFolder("tmp/666/666"), 
            before create mutiple-level-folder, you must sure the paretent-folder is existed, 
            in above case, you should make sure the path tmp/666 is already existed, otherwise, it will throw error
        """
        # ==== check whether parent-folder exist or not ====
        if newFolderName.count("/") >= 1:
            tmp = newFolderName.split("/")
            tmp.pop()
            parent = "/".join(tmp)

            parentfolders:list = self.find(parent, sort="folder")
            
            if len(parentfolders) < 1:
                assert False, "[Error] Parent folder not existed"

        # ==== DO create folder based on find result ====
        # find targeted-remote-folderpath
        folders:list = self.find(newFolderName, sort="folder")
        megaLogger.debug(f"newFolderName: {newFolderName}")
        megaLogger.debug(f"folders: {folders}")

        if len(folders) < 1: 
            # ==== folder not exist ====
            if not ("Root" in newFolderName):
                newFolderPath = "/Root/"+newFolderName

            cmd = f"megatools mkdir {newFolderPath} -u {self.user} -p {self.pwd}"
            self._exec(cmd)
            megaLogger.debug("folder created ...")

            # update file-system
            self.listFiles(display=False, reload=True)

            return newFolderPath

        elif len(folders) > 1:
            # ==== multiple folder ====
            assert False, f"[Error] mutiple folder existed ...\nGot folders: {folders}"
        
        else:
            # ==== already existed ====
            megaLogger.debug("folder already existed ...")
            return folders[0]

    def getFileLink(self, fn:str) -> str:
        """
        generate a public link to the specific filename
        
        要對資料夾中的所有檔案，個別產生 public-link，改用 megatools ls --export
        此方法只對資料夾內的檔案有效，不會產生資料夾產生 public-link
        """
        
        megaLogger.debug(f"filename: {fn}")
        megaLogger.debug(f"filelist: {self.fileList}")

        remotePath = self.find(fn, display=False, sort="file")
        assert len(remotePath) == 1, f"[Error] no|mutiple file found ...Got remotePate: \n{remotePath}"

        cmd = ["megatools", "export", remotePath[0], "-u", self.user, "-p", self.pwd]
        links:list = self._exec(cmd, display=False)

        assert len(links) == 1, f"[Error] no|mutiple link found ...Got links: \n{links}"

        return links[0]

    def _get_path(self, folderPath:str) -> str:
        if ":" in folderPath:
            # ==== absolutive path ====
            # Ex，c:/123
            return folderPath
        else:
            # ==== relatived path ====
            folderPath = Path.cwd() / folderPath
            
            if not folderPath.exists():
                os.mkdir(folderPath)
            
            return folderPath

    def download(self, fn:str, localPath:Optional[str]=None) -> None:
        """
        download file by specific filename

        Cause: 
            - Make sure localPath is exist
        
        Usage:
            megatools get /Root/flutter/123.txt
            mymega.download("333.txt", "ttt")
            #mymega.download("333.txt")，save file to project root
        """
        self.listFiles(display=False, reload=True)

        remotePaths:list = self.find(fn, display=False)
        assert len(remotePaths) == 1, f"[Error] no|multiple file existed, support file only\n{remotePaths}"

        cmd = f"megatools get {remotePaths[0]} -u {self.user} -p {self.pwd}"

        if localPath:
            localPath = self._get_path(localPath)
            cmd += f" --path {localPath}"

        self._exec(cmd)

    def downloadAll(self, remoteFolderName:str, localPath:Optional[str]=None) -> None:
        """
        download all files in folder to project root
        You can provider remoteFolderName only or complete full-path-folerName, both options are acceptable

        Usage:
            megatools copy --local <local-folder-name> --remote <remote-folder-path> --download
            downloadAll('tmp')
            downloadAll('/Root/tmp')
            downloadAll('/Root/tmp', localPath="ttt")
        """
        files:list = self.find(remoteFolderName, display=False)
        files.pop(0)
        
        for file in files:
            self.download(file, localPath=localPath)

    def upload(self, filePath:str, rFolderName:str, newName=None) -> str:
        """
        - Upload one file to mega cloud, if you need upload mutiple files, use `megatools.exe copy` instead
        - file-size CAN NOT be zero, otherwise, megatools will throw `Mega didn't return an upload handle`

        Usage
            megatools put --path /Root/README.TXT README
            mymega.upload("666.txt", "ttt")
        """
        # before upload, refresh file-system
        self.listFiles(display=False, reload=True)

        rPath:list = self.find(rFolderName, sort="folder")
        megaLogger.debug(f"fileList: {self.fileList}")
        
        assert len(rPath) == 1, f"[Error] no|multiple remote-folder-path existed, Got rPath:\n{rPath}"
        
        if newName:
            rPath += newName

        #cmd = f"megatools put {filePath} --path {rPath[0]} -u {self.user} -p {self.pwd}"
        # to avoid space in {filePath}, set cmd to array-format
        megaLogger.info("==== start upload file ====")
        cmd = ["megatools", "put", f"{filePath}", "--path", f"{rPath[0]}", "-u", f"{self.user}", "-p", f"{self.pwd}"]
        
        results = self._exec(cmd, display=True)

        if len(results) < 1:
            megaLogger.info(f"{filePath} upload fail ...")

        # after upload, refresh file-system
        self.listFiles(display=False, reload=True)

        return rPath[0]

    def uploadAll(self, folderPath:str, rFolderName:str):
        """
        - Upload folder into mega clound
        - Caution, this function will skip upload if file name is existed, and will not check file modified date.

        Usage:
            megatools copy 上傳時，只會檢查檔名，不會檢查檔案內容，若檔名重複，會出現 ERROR: File already exists at /Root/ttt/222.txt
            上傳，megatools copy --local sync --remote /Root/ttt -u chinghua0731+mega@gmail.com -p q1w2e3r4t5zaxscdvfbg --reload

            megatools copy 下載時，只會檢查檔名，不會檢查檔案內容，若檔名重複，會出現 ERROR: File already exists at /Root/ttt/222.txt
            下載，megatools copy --local sync --remote /Root/ttt -u chinghua0731+mega@gmail.com -p q1w2e3r4t5zaxscdvfbg --download

            mymega.uploadAll("upload", "ttt")
        """
        self.listFiles(reload=True, display=False)

        rFolderPath = self.find(rFolderName, sort="folder", display=False)
        assert len(rFolderPath) == 1 , f"[Error] no|multiple remote folder found\n {rFolderPath}"

        #cmd = f"megatools copy --local {folderPath} --remote {rFolderPath[0]} -u {self.user} -p {self.pwd} --reload"
        cmd = ["megatools", "copy", "--local", f"{folderPath}", "--remote", f"{rFolderPath[0]}", "-u", f"{self.user}", "-p", f"{self.pwd}", "--reload"]
        self._exec(cmd)

    def sync(self, localFolderName:str, remoteFolderName:str):
        """
        Sync one local folder to remote, this function will run uploadAll first then downloadAll, existed file will be ignore
        
        CAUTION:
            sync() only compare the file name, and will not compare the content in file. 
            So the file with same file name might have DIFFERENT content.
        TODO:
            need file information by megatools ls --long, if file is newer, then do upload/download
        
        Usage:
            mymega.sync(localFolderName="upload", remoteFolderName="ttt")
        """
        remoteFolderPath:list = self.find(remoteFolderName, sort="folder")
        assert len(remoteFolderPath) == 1, "[Error] no|multiple remote folder path found"

        self.uploadAll(localFolderName, remoteFolderPath[0])
        self.downloadAll(remoteFolderPath[0], localFolderName)

    def _write2DB(self):
        pass

    def _reg(self) -> Optional[dict]:
        megaLogger.info("==== reg and verify ====")

        mega_key:Optional[str] = None
        verify_link:Optional[str] = None
        result:Optional[dict] = None

        # ==== step1，generate information ====
        name = genPass(6, digit=False)
        mail = genMail()
        pwd = genPass()

        megaLogger.info(f"NAME:{name}, MAIL:{mail}, PWD: {pwd}")

        try:
            # ==== step2，execute mega reg, and get mega_key ====
            cmd = f"megatools reg --register --email {mail} --name {name} --password {pwd}"
            result = self._exec(cmd, display=False)
            assert len(result) == 1, f"[Error] No key found ... Got result=\n{result}" 

            mega_key = result[0]
            megaLogger.debug(f"mega_key: {mega_key}")

            # ==== step3，check mail ====
            nada = Pynada()

            megaLogger.info("wait 5 second to get verify email ...")
            time.sleep(5)
            
            for email in nada.inbox(mail).get_emails():

                if email.subject == "驗證您的MEGA帳號註冊郵箱" :
                    doc = pq(email.get_contents())
                    
                    el = doc("#main-pad > p > a")

                    verify_link = el.attr["href"]
                    megaLogger.debug(f"verify_link: {verify_link}")

                    email.delete()
            
            # ==== execute mega reg, and get mega_key ====
            cmd = f"megatools reg --verify {mega_key} {verify_link}"
            self._exec(cmd)

            result = {"name":name, "mail":mail, "pwd": pwd}

        except Exception as e:
            megaLogger.debug(e)
            assert False, "[Error] register account fail ..."

        return result

    def reg(self, n:int=1) -> Union[str, dict]:
        results:list = []
        accountInfo:dict = {}

        for i in range(n):
            accountInfo = self._reg()
            results.append(accountInfo)

        if len(results) == 1:
            return results[0]
        else:
            return results

def test_mymegatools():
    mega = MyMegaTools(executable=Config.executable, user=Config.user, pwd=Config.pwd)

if __name__ == "__main__":
    pass
    #test_mymegatools()