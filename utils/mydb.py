
from base64 import b64encode, b64decode
from hashlib import md5
from collections import namedtuple
from typing import Optional, Union

from peewee import *

from utils.logger import dbLogger

db = SqliteDatabase('mega.db')

class Base(Model):
    class Meta:
        database = db

NewAccount = namedtuple("newAccount", "name mail pwd")
class Account(Base):
    an = TextField(verbose_name="username", null=False)
    am = TextField(index=True, verbose_name="mail", null=False)
    ap = TextField(verbose_name="password", null=False)
    act = DateTimeField(verbose_name="created-date", constraints=[SQL("DEFAULT (datetime('now'))")])

NewItem = namedtuple("newItem", "name md5 b64 remotePath remoteLink account")
class Item(Base):
    fn = TextField(verbose_name="filename", null=False)
    fmd5 = TextField(verbose_name="filename in md5", null=False)
    fb64 = TextField(verbose_name="filename in b64", null=False)
    fp = TextField(verbose_name="filepath at cloud", null=False, constraints=[ ])
    flink = TextField(verbose_name="link at cloud", null=False)
    account = ForeignKeyField(Account, backref="files", null=False)
    
class DbHandler:
    def __init__(self):
        db.create_tables([Account, Item])

    @staticmethod
    def _str2b64(filename:str) -> str:
        return b64encode(filename.encode("utf-8")).decode("utf-8").replace("/", "#")

    @staticmethod
    def _str2md5(filename:str) -> str:        
        m = md5()
        m.update(filename.encode("utf-8"))
        
        return m.hexdigest()

    def getDataRow(self, name:str, table:str) -> Optional[Model]:
        table = table.lower()

        if table == "account":
            return Account.get_or_none(Account.an == name)
        elif table == "item":
            return Item.get_or_none(Item.fn == name)
        else:
            raise ValueError(f"[Error] Unknown table: {table}")

    def isRowExist(self, name:str, table:str) -> bool:
        return True if self.getDataRow(name, table) else False

    def removeRow(self, name:str, table:str) -> bool:
        """
        Usage: db.removeRow(name="faa", table="item")
        """
        target:Model = self.getDataRow(name, table)

        if target: 
            target.delete_instance()
            dbLogger.debug(f"{table} deleted")
        else:
            dbLogger.debug(f"{table} not found")

    def addRow(self, newData:Union[NewAccount, NewItem], table:str) -> Optional[Model]:
        """
        Caution: 
            check duplicate by name only
        Usage:
            db.addRow(Account(name="aa", mail="123@gmail.com", pwd="123"), table="account")
            db.addRow(Account(name="aa", mail="123@gmail.com", pwd="456"), table="account")     # already existed
            db.addRow(Account(name="an", mail="123@gmail.com", pwd=None), table="account")      # add failed
        """
        # == 檢查紀錄是否已經存在 ==
        row = self.getDataRow(newData.name, table=table) 

        # == 紀錄不存在，寫入新紀錄 == 
        if not row:
            try: 
                if table == "account":
                    row = Account.create(an = newData.name, am = newData.mail, ap = newData.pwd)
                elif table == "item":
                    row = Item.create(
                        fn=newData.name , fmd5=newData.md5, 
                        fb64=newData.b64, fp=newData.remotePath, 
                        flink=newData.remoteLink, account=newData.account,
                    )
                else:
                    raise ValueError(f"[Error] Unknown table: {table}")
                
                return row

            except Exception as e:
                dbLogger.info(f"{table} add failed ...")
                dbLogger.info(f"[Error] {e}")
                return None
        
        # == 紀錄已存在== 
        else:
            dbLogger.info(f"{table} already existed ...")
            return None

def main():
    db = DbHandler()
    
    db.addRow(NewAccount(name="aa", mail="123@gmail.com", pwd="123"), table="account")
    db.addRow(NewAccount(name="bb", mail="456@gmail.com", pwd="456"), table="account")

    aa = db.getDataRow("aa", table="account")
    db.addRow(NewItem(name="faa", md5="faa", b64="faa", remotePath="faa", remoteLink="faa", account=aa), table="item")
    db.addRow(NewItem(name="fbb", md5="fbb", b64="fbb", remotePath="fbb", remoteLink="fbb", account=aa), table="item")

    db.removeRow(name="faa", table="item")
    db.removeRow(name="666", table="item")

if __name__ == "__main__":
    main()