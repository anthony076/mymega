"""
Usage of mega.py

t 階層，t = 0 最底層
    例如，
    aa\bb\123.zip
    aa -> t=2
    bb -> t=1
    123.zip -> t=0


a-key，檔案相關資訊
    資料夾未設定連結
        資料夾或檔案類型時，a-key 的類型是 list
        未設定連結的檔案才會有 c-key，有c-key才會有檔案名

    資料夾有設定連結
        資料夾下的所有檔案，a-key 的類型是 String
        有設定連結的檔案不會有 c-key，沒有c-key就`沒有檔案名`
        格式，https://mega.nz/folder/ykplRK5R#tP-KU2Z8unT3ToDyEHoMeg/file/<a-key>

p-key，檔案所在的目錄，會保存在 p-key， p-key 保存的是，對應所在目錄的 key
    例如，
    image/ 123.png，imges目錄對應的 key 是 iii
    則 123.png 檔案的 p-key 保存的是 iii

h-key，當前 value 對應的 key
    例如，
    "foo":{ "a":1, "b":2, "h":"foo"}

"""

import json
from mega import Mega
from typing import Optional, Union

def pp(d):
    jsonStr = json.dumps(d, sort_keys=True, indent=2)
    fp = open("log", "w")
    fp.write(jsonStr)
    fp.close()

class MyMega:
    def __init__(self):
        self.sess: Optional[Mega] = None
        self.fileList = []

    def login(self, email:str, pwd:str):
        try:
            self.sess = Mega().login(email, pwd)
            print(f"{email} login success ...")
        except:
            print(f"{email} login fail ...")

    def userInfo(self) -> dict:
        info:dict = self.sess.get_user()
        print(info)
        return info

    def diskInfo(self) -> dict:
        return self.sess.get_storage_space(giga=True)

    def listFiles(self, debug:bool=False) -> list:
        filelist:list = []
        folderNameMap:dict = {}

        d:dict = self.sess.get_files()
        if debug: pp(d)

        # ==== get filelist 和 folderNameMap from raw-dict ====
        for rowKey in d.keys():
            infos:Optional[str, dict]= d[rowKey]["a"]

            if isinstance(infos, str):
                pass

            if isinstance(infos, dict):
                isFile:bool = infos.get("c")

                if (isFile):
                    filelist.append( {"filename":infos["n"], "folder": d[rowKey]["p"]} )
                else:
                    folderNameMap[rowKey] = infos["n"]

        # ==== replace folder in filelist ====
        for fileMap in filelist:
            fileMap["folder"] = folderNameMap[fileMap["folder"]]
            self.fileList.append( fileMap["folder"] + "/" + fileMap["filename"])
        
        self.fileList.sort()

        print(self.fileList)

        return self.fileList

    def find(self, fn:str) -> Optional[dict]:
        """
        m.find("vplayer.zip")  # return file-object
        m.find("vplayer")       # return None
        """
        return self.sess.find(fn)

    def download(self, target:str):
        """
        m.download("vplayer.zip")
        m.download("https://mega.nz/file/S8gRmQQb#WrHwKw-lQEROU-kx7l3hPnTrvD3brPPE6vp2_6Lpk00")
        """
        assert target, "target parameter cant be None"
        
        isUrl:bool = True if "http" in target else False

        try:
            if isUrl:
                self.sess.download_url(target)

            else:
                fo:dict = self.sess.find(target)
                self.sess.download(fo)

        except PermissionError:
            pass
    
    def upload(self, fn:str, path:str) -> str:
        f = self.sess.upload(fn, self.sess.find(path)[0])
        return self.sess.get_upload_link(f)

def test_main():
    m = MyMega()
    m.login("", "")
    #print(m.upload("ttt.zip", "tmp"))
    m.listFiles(debug=True)
    

if __name__ == "__main__":
    test_main()